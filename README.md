# README #
Design a classifier to use for sentiment analysis of product review. 

### What is this repository for? ###

The training set consists of reviews written by Amazon customers for various food products. The reviews is on a +1 or -1 scale, representing a positive or negative review, respectively.
Implemented and compared three types of linear classifier: Perceptron, Average Perceptron, Pegasos.

### How do I get set up? ###
project1.py contains various functions and learning algorithms.

main.py is a script where these functions are called, to run experiments.

utils.py contains utility functions.

test.py is a script which runs tests on a few of the methods  to help debug implementation.
